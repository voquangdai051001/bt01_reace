import React, { Component } from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import Item from "./Item";

export default class Demo extends Component {
  render() {
    return (
      <div>
        <div style={{ backgroundColor: "#212529" }}>
          <div className="container">
            <Header />
          </div>
        </div>

        <div className="p-4 container">
          <Banner />
        </div>
        <div className="container pb-5">
          <div className="row">
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}
