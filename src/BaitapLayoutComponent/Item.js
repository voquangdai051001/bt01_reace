import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div className="pt-5 pt-5">
        <div className="card" style={{backgroundColor:'#f8f9fa', width: "100%", padding:'48px', position:'relative', alignItems:"center"}}>  
        <div className="bg-primary" style={{fontSize:'40px', position:'absolute', top:"-40px", width:'60px', borderRadius:'8px', color:"white"}}>
          <i class="fab fa-500px"></i>  
        </div>    
          <div className="card-body p-0">
            <h4 style={{fontWeight:'700'}} className="card-title">Fresh new layout</h4>
            <p className="card-text">
            With Bootstrap 5, we've created a fresh new layout for this template!
            </p>
          </div>
        </div>
      </div>
    );
  }
}
