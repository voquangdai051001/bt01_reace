import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
        <div style={{backgroundColor:'#f8f9fa', paddingBlock:'48px'}} class="card">
          <div class="card-body" style={{padding:'48px'}}>
            <h1 style={{fontWeight:'700'}}>A warm welcome!</h1>
            <h3 style={{fontWeight:'400'}}>Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</h3>
            <a href="#" class="btn btn-primary">Call to action</a>
          </div>
        </div>
    );
  }
}
