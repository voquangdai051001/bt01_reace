import React, { Component } from "react";
import "./style.css";
export default class Header extends Component {
  render() {
    return (
      <div id="header">
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
          class="navbar navbar-expand-lg navbar-light "
        >
          <div class="">
            <a
              style={{ textDecoration: "none", fontSize: "25px" }}
              className="text-white"
              href="#"
            >
              Start Bootstrap
            </a>
          </div>
          <div>
            <ul style={{ color: "white", textDecoration: "none", margin: "0" }}>
              <li
                className="pl-3"
                // style={{ display: "inline", cursor: "pointer" }}
              >
                Home
              </li>
              <li className="pl-3 hover">About</li>
              <li className="pl-3 hover">Contact</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
